---
title: "Présentation de l'événement du 15 juin?"
date: 2022-06-01
tags: ["Cooperation", "Agriculture", "Alimentation", "Circuits courts"]
featured: "home"
---

???
<!--more-->

???
---

→ Si vous souhaitez participer à nos actions, n’hésitez pas à [adhérer à Bretagne Transition](https://www.helloasso.com/associations/bretagne-transition/adhesions/bulletin-d-adhesion-a-l-association-bretagne-transition) et nous contacter : [contact@bretagne-transition.org](mailto:contact@bretagne-transition.org)

→ [Voir toutes nos actions](/actions/)
