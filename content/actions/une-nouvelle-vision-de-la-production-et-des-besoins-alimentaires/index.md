---
title: "Une nouvelle vision de la production et des besoins alimentaires"
date: 2022-02-15
tags: ["Coopération", "agriculture", "alimentation", "Circuits courts"]
featured: "home"
---

Mardi 8 février, Bretagne Transition a rassemblé consommateurs, producteurs, AMAP et associations pour un apéro-rencontre sur le thème de l’alimentation, des circuits-courts et des groupements d’achat.
<!--more-->

L’objectif était de rassembler des personnes concernées par le développement d’une nouvelle vision des besoins alimentaires et des conditions de production, de transformation et de distribution à l’échelle locale.

De nombreux points ont été abordés comme la difficulté de mise en relation des producteurs et des consommateurs à une échelle très locale et des circuits courts de distribution qui restent encore limités. Le besoin de résilience alimentaire de l’agglomération a été soulevé avec l’idée d’une Sécurité Sociale Alimentaire ou de nouvelles structures coopératives autogérées. De nouveaux moyens de production restent à inventer comme par exemple un réseau de parcelles collectives permettant la mise en commun des outils. Mais de nombreuses initiatives pour encourager l’installation de nouveaux agriculteurs posent l’éternel problème de l’accès au foncier.

## Les actions

+ Continuer à coordonner les collectifs de réappropriation alimentaire
+ Mettre à dispo une cartographie des acteurs des circuits court
+ Communiquer pour offrir des opportunités aux producteurs qui veulent vendre en circuit court
+ Participer à l’élaboration du Projet Alimentaire Territorial (PAT) sur le territoire de CCA

Bretagne Transition va, avec l'ensemble des acteurs impliqués, continuer tout au long de cette année 2022 à créer du lien et proposer des événements de sensibilisation et de formation sur l’évolution nécessaire du régime alimentaire, de sa production, de sa transformation et de sa distribution.

---

→ Si vous souhaitez participer à nos actions, n’hésitez pas à [adhérer à Bretagne Transition](https://www.helloasso.com/associations/bretagne-transition/adhesions/bulletin-d-adhesion-a-l-association-bretagne-transition) et nous contacter : [contact@bretagne-transition.org](mailto:contact@bretagne-transition.org)

→ [Voir toutes nos actions](/actions/)
