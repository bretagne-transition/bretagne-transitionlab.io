---
title: À propos de Bretagne Transition
date: 2021-12-18
draft: true

---

DRAFT

Afin d’anticiper les nombreux défis à venir de façon positive,
Bretagne Transition accompagne les acteurs du territoire en faveur d’une transition citoyenne durable et équitable. Pour cela, l’association apporte des éléments de compréhension de ces enjeux et facilite le partage d’expériences innovantes concrètes ancrées sur les territoires. Elle organise des conférences et ciné-débats avec des professionnels, des balades et visites pour découvrir de nouveaux modes de fonctionnement de la société, des évènements de diffusion et mise à disposition de l'information pour entamer la réflexion...

## Comprendre les enjeux et problèmes à dépasser

Questionner notre mode de développement actuel à fort impact environnemental et social, et identifier les alternatives qui y répondent au travers de conférences thématiques.

## Imaginer des territoires créateurs de valeurs fortes

Se projeter dans un futur souhaitable et tracer les chemins pouvant y conduire au travers de forums ouverts.

## Mobiliser des savoirs et des techniques innovantes

Au travers d'ateliers, d'activités de découvertes et d'outils collaboratifs.

## Expérimenter pour donner l'envie d'agir

En se donnant le droit à l'erreur, et en restant ouverts à de nouvelles connaissances
