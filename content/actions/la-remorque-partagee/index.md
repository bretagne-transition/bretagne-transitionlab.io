---
title: "La remorque partagée"
date: 2021-12-19
tags: ["Cooperation", "Mobilites", "Circuits courts"]
featured: "none"
---

Ce projet aujourd'hui emblématique fait suite aux rencontres de Kervic sur les mobilités, ainsi qu'aux premiers ateliers d'échange entre acteurs locaux des mobilités ; il est largement inspiré de l'exemple de Cargonomia en Hongrie.

<!--more-->

Il a d'abord consisté en une enquête approfondie en local, et en une étude du besoin et des opportunités ; puis dans la réalisation (dans le cadre de formations à l'autoconstruction) d'un prototype fonctionnel de remorque à vélo avec assistance électrique ; et finalement dans la mise en place d'un outil libre de gestion en commun de cette remorque, entre de 10 premiers acteurs impliqués, et ce afin répondre de façon appropriée, locale, soutenable et économe à leurs besoins ponctuels en logistique. La suite de l'expérimentation reste à co-construire avec CCA ainsi que d'autres acteurs économiques historiques.

Ce projet est l’occasion de faire du lien entre les producteur·rices et commerçant·es, CCA, le KAL, le CRADE, Bélo Livraison ou Finistère Habitat, et les habitant·es de Concarneau. Il répond à des enjeux environnementaux tout en présentant une forte composante sociale et solidaire. De plus, c’est un véritable outil de sensibilisation et de promotion autour de ces nouveaux usages.

## Un bien commun

Les associations et commerçants concarnois ont ponctuellement besoin de transporter du matériel ou des aliments dans le cadre de leurs activités et événements, or ils elles ne possèdent pas toujours de véhicule pour le faire. Ce projet a consisté en la mise à disposition d'une remorque à vélo à assistance électrique. Un bien commun qui est d'ores et déjà géré par les parties-prenantes elles-mêmes. L'année 2022 sera dédiée à une première phase d'expérimentation et de suivi.

## Un prototype fonctionnel

C'est Clara, dans le cadre de son stage et avec l'assistance de Quentin déjà administrateur, qui a été rencontrer les différents acteurs, producteurs, potentiels bénéficiaires, et a coordonné d'une main de maître les études de besoin et d'opportunités. Elle a continué de mener le projet en participant, avec Paul, alors ancien service-civique et membre actif bénévole, à un stage de formation à l'auto-construction de la remorque. Cette dernière a été présentée et éprouvée à différents acteurs du territoire pendant le Low-Tech Tour de juillet 2021.
La remorque partagée pendant le Low-Tech Tour

## Les partenaires

+ Cargonomia & Véloa
+ La Fabrique des Mobilités
+ Le Konk-Ar-Lab
+ Le Low-tech Lab
+ La Petite Ferme de Kercaudan
+ L'AMAP de Concarneau
+ L'association des commerçants
+ KKS & les restaus du cœur
+ Belo Livraison & Coopcycle
+ Le CRADE
+ Rehab
+ La Konk Creative
+ Finistère Habitat (pour le local)

---

→ Si vous souhaitez participer à nos actions, n’hésitez pas à [adhérer à Bretagne Transition](https://www.helloasso.com/associations/bretagne-transition/adhesions/bulletin-d-adhesion-a-l-association-bretagne-transition) et nous contacter : [contact@bretagne-transition.org](mailto:contact@bretagne-transition.org)

→ [Voir toutes nos actions](/actions/)
