---
title: "Une rencontre des acteurs de la mobilité de CCA"
date: 2022-05-01
tags: ["Coopération", "Mobilites", "Circuits courts"]
featured: "home"
---

L’association Bretagne Transition soutient l’ensemble des associations, collectifs et instances qui participent au développement de la cyclabilité et des modes de déplacements alternatifs sur le territoire de Concarneau Cornouaille Agglomération.
<!--more-->

C’est dans un esprit de coopération que nous avons organisé le 27 avril un apéro convivial à la Maison des associations avec celles et ceux qui s’investissent localement sur le sujet, pour s’informer sur les initiatives passées et les projets en cours, partager envies, freins, besoins et coordonner nos actions.

<figure>
<img src="apero-mobilite-01.jpg" alt="Une bande de joyeux cyclistes en pleine stratégie de conquête">
<figcaption>Une bande de joyeux cyclistes en pleine stratégie de conquête.</figcaption>
</figure>

Les collectifs représentés étaient [OzActes](https://www.facebook.com/pages/category/Nonprofit-organization/OzActes-110467843733196/), Vélo228, [Collectif Concarneau Solidaire et Durable](https://csd.bzh/), [l’Atelier Z](https://fr-fr.facebook.com/pages/category/Local-business/Atelier-Z-1208175105890689/) et La Fabriq’ de Névez, [le Konk Ar Lab](https://www.konkarlab.bzh/), [le CRADE](https://lecrade.fr/) et les Conseils de Quartier de Lanriec, de quartier du centre ville et des Sables-blancs.

## Les actions envisagées

+ Développer une vision globale à l’échelle du territoire.
+ Porter des revendications, messages, solliciter les personnes publiques par l’intermédiaire du collectif d’asso pour avoir davantage d’impact.
+ Imaginer un nouveau mode de communication pour coordonner les actions.
+ Assurer une meilleure représentation aux réunions d’élaboration du SCOT.
+ Contacter la commission mobilité de CCA.
+ Faire valoir le succès des actions d’OzActes leur ayant permis de récupérer un tronçon de route en faveur des vélos.
+ Récupérer auprès de Concarneau et de CCA les documents, schémas, diagnostics réalisés dans le passé qui nous permettraient de mieux appréhender l’état de la cyclabilité.
+ Se mettre en lien avec les associations de randonneurs.
+ Mutualiser et communiquer le calendrier des évènements.

## Les événements à venir

**Samedi 18 juin - Friture sur la ligne**. Un événement culturel, concerts le long de la voie verte de Rosporden.

**Samedi 2 juillet - Vélorution dans Concarneau** à la fin du [Festival Low-tech](https://lowtechlab.org/fr/le-low-tech-lab/les-actions/festival-low-tech-du-25-juin-au-3-juillet-2022).

**Septembre - La roulade** Manifestation à vélo pour faire suite à l’ouverture de la voie verte entre Riec et Pont-Aven qui a pour objectif la création d’aménagements cyclables. Plus d'info à venir.

---

→ Si vous souhaitez participer à nos actions, n’hésitez pas à [adhérer à Bretagne Transition](https://www.helloasso.com/associations/bretagne-transition/adhesions/bulletin-d-adhesion-a-l-association-bretagne-transition) et nous contacter : [contact@bretagne-transition.org](mailto:contact@bretagne-transition.org)

→ [Voir toutes nos actions](/actions/)
