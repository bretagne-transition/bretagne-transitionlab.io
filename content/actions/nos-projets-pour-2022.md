---
title: "2022, une nouvelle année de coopération et de projets"
date: 2021-12-17
tags: ["Cooperation", "Communication"]
draft: false

---

Le contexte délicat du Covid et de ses différents confinements ont impacté les différentes activités de l'association mais a également confirmé la justesse et l'urgence de l'ensemble de nos projets afin de contribuer à la nécessaire transition du territoire vers toujours plus de soutenabilité, de justice environnementale et d'autonomie ou de résilience.

<!--more-->

Bretagne Transition développe et anime un réseau d'acteurs toujours plus dense et engagé mais nos actions ne se limitent pas au territoire. Nous cherchons toujours à développer des partenariats à toutes les échelles : de modestes associations de citoyens aux institutions françaises ou européennes responsables des politiques publiques en passant par les communes et la région. Nous développons aussi des programmes d'exploration, d'expérimentation et de coopération entre différents territoires. Les partenariats avec la Fabrique des mobilités et la Fabrique des énergies, ou bien les liens avec le Pays du Bocage ornais, ou le Pays du Bocage bressuirais vont dans ce sens.

Un des points forts de ces 2 années passées a été le projet de cyclo-logistique, la remorque partagée, qui a contribué au montage d'une coopérative de coursiers (commerciale et solidaire) sur le territoire de CCA, abouti à la production d'un prototype fonctionnel — véritable emblème ! — et à une expérimentation qui reste à co-construire, permis la mise en place d'un commun technique et d'un outil de partage. Ce projet réaffirme la volonté locale de soutenir, développer, promouvoir une mobilité et une logistique plus soutenables, de dynamiser les circuits courts et de créer toujours plus de liens entre les structures et les citoyens. C'est avec cette approche systémique et ces modalités d'enquête, de coopération, de prototypage, d'expérimentation, et d'accompagnement au développement économique, que Bretagne Transition inscrit désormais ses actions.

Pour cette nouvelle année 2022 l'ensemble des projets de l'association s'organiseront autour de quatre grands pôles — Mobilités & Logistique, Habitats & Réemploi, Énergies & Low-tech, Agricultures & Circuits courts — et mobiliseront toujours plus de rencontres, de synergies, de coopération, afin de sensibiliser, d'inspirer et d'accompagner l'ensemble des acteurs du territoire dans leur transition, leur adaptation, leur transformation, en réponse aux grandes problématiques auxquelles la nous nous trouvons collectivement confrontés.

-- La collégiale Bretagne Transition

---

→ [Voir toutes nos actions](/actions/)
