---
title: "La mutualisation de la remorque partagée"
date: 2022-04-01
tags: ["Cooperation", "Mobilites", "Circuits courts"]
featured: "home"
---

Le 23 mars, Bretagne Transition a organisé un apéro pour la mutualisation de la remorque partagée.
<!--more-->

Étaient représentés le [Low-Tech Lab](https://lowtechlab.org/fr), [Belo Livraison](https://belolivraison.fr/), [le CRADE](https://lecrade.fr/) et [Bretagne Transition](https://bretagne-transition.org)

Après avoir actualisé les usages prévues par les associations présentes, nous avons pu identifier des points à clarifier pour que [la mutualisation de la remorque](/actions/la-remorque-partagee/) roule au mieux ! L’objectif aujourd’hui est de remettre en place les outils numériques. Un calendrier où chacun.e peut ajouter ses projets de réservation de la remorque et un trombinoscope pour que chacun.e puisse se contacter directement. Bretagne Transition va faire une étude d’impact de cette expérimentation pour pouvoir répliquer ce type de fonctionnement.

A la fin de l’apéro, certain.es ont pu tester la remorque. Pour une première utilisation, le mieux est de prendre le temps de faire des tours de parking pour bien appréhender la remorque , sa taille et son fonctionnement. Il est possible d’appeler Paul ou Robin de Bretagne Transition pour se faire accompagner à la prise en main. Un guide d’utilisation à jour sera bientôt disponible pour avoir toutes les informations.

Un grand merci au CRADE pour son accueil.

---

→ Si vous souhaitez participer à nos actions, n’hésitez pas à [adhérer à Bretagne Transition](https://www.helloasso.com/associations/bretagne-transition/adhesions/bulletin-d-adhesion-a-l-association-bretagne-transition) et nous contacter : [contact@bretagne-transition.org](mailto:contact@bretagne-transition.org)

→ [Voir toutes nos actions](/actions/)
