---
title: "Mise en lien entre restaurations collectives et producteurs locaux"
date: 2022-05-15
tags: ["Cooperation", "Agriculture", "Alimentation", "Circuits courts"]
featured: "home"
---

Interview de Charlotte Guiet, stagiaire à Bretagne Transition sur la mission “Mise en lien entre restaurations collectives et producteurs locaux”
<!--more-->

**BT - D’où viens-tu ? Quel est ton parcours ?**

CG - J’ai grandi à Annecy en Haute-Savoie, mais j’ai choisi de faire un Master intitulé “Expertise et Gestion de l’Environnement Littoral” à Brest. C’est une formation pluridisciplinaire avec à la fois des cours de géographie humaine, d’économie, de droit de l’environnement, mais aussi sur les sciences marines (biologie, chimie, écologie).

**BT - Pourquoi as-tu choisi de travailler avec Bretagne Transition ?**

CG - Cela faisait quelque temps que je voulais travailler sur un sujet lié à l’agriculture durable et les circuits-courts. J’ai d’abord été en contact avec Quentin Mateus via le Low tech Lab de Concarneau qui m’a parlé des projets de Bretagne Transition, notamment celui concernant les circuits-courts dans les restaurations collectives.

**BT - En quoi consiste ta mission précisément ?**

CG - Mon objectif a été dans un premier temps d’aller rencontrer un maximum de personnes faisant partie de la filière alimentaire locale, notamment des agriculteurs, des pêcheurs, des artisans/transformateurs ainsi que des cuisiniers de restauration collective. Ils m’ont fait part de leurs contraintes, de leurs attentes vis -à -vis du circuit-court en général. Je suis actuellement en train d’organiser des temps de rencontre entre producteurs, mais aussi entre  restaurations collectives et élus du territoire concernés par la question de l’approvisionnement en circuit court. Le but est de faire un bilan des freins à la coopération entre restaurations collectives et producteurs locaux, puis de réfléchir collectivement à des leviers d’action pour faciliter la restauration collective bio et locale.

**BT - Avec qui travailles-tu ?**

CG - Je travaille beaucoup avec les autres bénévoles de BT, notamment avec Romane Cadars qui est ma tutrice pour ce stage, et qui me guide vers les bonnes personnes. Je suis aussi en contact avec l’Atelier Z, un tiers-lieu convivial qui accueille des porteurs de projets, des ateliers, des événements et qui permet à qui veut de venir bricoler. Je discute également avec plusieurs personnes faisant partie d’Explore et notamment du Low Tech Lab. Ce qui est important c’est de s’entourer des personnes qui connaissent bien le territoire.

**BT - Que retiens-tu de ton stage pour le moment ?**

CG - C’est un stage hyper enrichissant, d’abord parce que je rencontre plein de monde et que j’apprends de leur expérience/métier, et aussi parce que je réalise en même temps toutes les difficultés auxquelles se heurte ce type de projet. Ce que je retiens de mon expérience pour l’instant, c’est qu’il faut en priorité rassembler et faire discuter les acteurs entre eux. Non seulement pour qu’ils s’écoutent, qu’ils comprennent les contraintes des uns et des autres mais aussi et surtout pour écrire collectivement un nouveau récit concernant le système alimentaire de notre territoire.

<figure>
<img src="serre-resto-co-01.jpg" alt="Serre de la Lande Fertile, Boulangerie E’copain et Cuisine collective scolaire à Saint-Yvi">
<figcaption>Serre de la Lande Fertile, une exploitation maraîchère bio à dont les légumes sont destinés aux restaurations collectives à Moëlan-sur-Mer. La boulangerie E’copain à Bannalec et une cuisine collective scolaire à Saint-Yvi.</figcaption>
</figure>

---

→ Si vous souhaitez participer à nos actions, n’hésitez pas à [adhérer à Bretagne Transition](https://www.helloasso.com/associations/bretagne-transition/adhesions/bulletin-d-adhesion-a-l-association-bretagne-transition) et nous contacter : [contact@bretagne-transition.org](mailto:contact@bretagne-transition.org)

→ [Voir toutes nos actions](/actions/)
