---
title: "Rencontrons-nous aux moments forts de l'été"
date: 2022-06-05
tags: ["Cooperation", "Mobilites", "Low-tech", "Energie", "Agriculture", "Alimentation", "Circuits courts"]
featured: "home"
---

Profitons des événements à venir pour nous rencontrer, échanger et coopérer pour une transition citoyenne, durable et équitable.

<!--more-->

Bretagne Transition sera présents aux événements suivants mais n'hésitez pas à [nous contacter](mailto:contact@bretagne-transition.org) pour organiser une rencontre.


## Le Low-Tech Festival

Du 25 juin au 3 juillet à Concarneau - Quai d’aiguillon

&rarr; Retrouvez [toutes les infos](https://lowtechlab.org/fr/festival-2022/le-festival-kesako) sur le site du *Low-Tech Lab*.

## 11e étape de « La Tournée des Tiers-Lieux, Les Rencontres Citoyennes de la Convention Climat »

Vendredi 1er juillet, Bretagne Transition participera à une table ronde organisée par [Le Grand Rebond](https://www.le-grand-rebond.fr/) réunissant acteurs économiques, associatifs et de l’ESS, afin de discuter des enjeux liés aux transitions. Cette table ronde se tiendra au sein de La Maison Glaz à Gâvres, de 14H à 15H30.

&rarr; Retrouvez [toutes les infos](https://www.latourneedestierslieux.fr/etapes/maison-glaz) sur le site de *la Tournée des Tiers-Lieux*.

## Les rencontres de Kervic 2022

Cinquième édition sur les fictions communes. Le festival aura lieu du vendredi 8 juillet 17h30 au samedi 9 juillet 22h à Kervic — Nevez.

&rarr; Retrouvez [les infos](https://az.atelierz.xyz/site/rencontres_de_kervic) sur le site de l'*Atelier Z*.

## Les Comices du Faire 2022

Du 23 au 30 juillet à l'Atelier Z de Nevez.

---

## N'oublions pas les chantiers participatifs

Premier chantier participatif sur le site de Kerbouzier à Melgven pour construire une paillourte, un habitat léger en terre/paille avec toit végétalisé dans une ambiance endiablé! Pour vous inscrire, [suivre ce lien](https://framaforms.org/chantier-participatif-dune-paillourte-a-kerbouzier-1652122708).

---

→ Si vous souhaitez participer à nos actions, n’hésitez pas à [adhérer à Bretagne Transition](https://www.helloasso.com/associations/bretagne-transition/adhesions/bulletin-d-adhesion-a-l-association-bretagne-transition) et nous contacter : [contact@bretagne-transition.org](mailto:contact@bretagne-transition.org)

→ [Voir toutes nos actions](/actions/)
