# Bienvenue sur Bretagne Transition

L'association Bretagne Transition accompagne les acteurs du territoire en faveur d’une transition citoyenne, durable et équitable. Elle apporte des éléments de compréhension autour de ces enjeux et facilite le partage d’expériences innovantes concrètes ancrées sur les territoires.
